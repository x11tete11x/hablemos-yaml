# Create Cluster

# Watch open files and max user instances

```
sudo sysctl fs.inotify.max_user_instances=1280
sudo sysctl fs.inotify.max_user_watches=655360
sudo sysctl fs.file-max=500000
```

# Tiny

```
k3d cluster create dev-tiny --api-port <SERVER IP>:6550 -p "8081:80@loadbalancer" -p "30000-30100:30000-30100@server:0" -p "30000-30100:30000-30100/UDP@server:0" --servers 1 --agents 3 --registry-create dev-tiny
```

# Big

```
k3d cluster create dev-big --api-port <SERVER IP>:6550 -p "8081:80@loadbalancer" -p "30000-30100:30000-30100@server:0" -p "30000-30100:30000-30100/UDP@server:0" --servers 1 --agents 10 --registry-create dev-big --timeout 10m
```

# Deploy

```
kubectl kustomize --enable-helm | kubectl apply -f -
```

Or using helm:

```
kubectl create ns monitoring
helm install metrics-server --values=values-metrics-server.yaml bitnami/metrics-server
helm install kube-prometheus --values=values-kube-prometheus.yaml bitnami/kube-prometheus
helm install kube-state-metrics --values=values-kube-state-metrics.yaml bitnami/kube-state-metrics
helm install cert-manager --values=values-cert-manager.yaml bitnami/cert-manager
helm install grafana --namespace monitoring --values=values-grafana.yaml bitnami/grafana
```

# Useful Tools

* [kubectl](https://kubernetes.io/docs/tasks/tools/#kubectl)
* [kubectx and kubens](https://github.com/ahmetb/kubectx)
* [kustomize](https://kubectl.docs.kubernetes.io/installation/kustomize/binaries/)
* [helm](https://helm.sh/docs/intro/install/)
* [openlens](https://github.com/MuhammedKalkan/OpenLens)

# Scale Cluster

## Add node
Currently looks like k3d scale node cluster it's not working good

```
k3d node create dev-tie-tt1-extra-0 --cluster dev-tie-tt1 --role agent
k3d node create dev-tie-tt1-extra-1 --cluster dev-tie-tt1 --role agent
k3d node create dev-tie-tt1-extra-2 --cluster dev-tie-tt1 --role agent
k3d node create dev-tie-tt1-extra-3 --cluster dev-tie-tt1 --role agent
k3d node create dev-tie-tt1-extra-4 --cluster dev-tie-tt1 --role agent
k3d node create dev-tie-tt1-extra-5 --cluster dev-tie-tt1 --role agent
k3d node create dev-tie-tt1-extra-6 --cluster dev-tie-tt1 --role agent
k3d node create dev-tie-tt1-extra-7 --cluster dev-tie-tt1 --role agent
k3d node create dev-tie-tt1-extra-8 --cluster dev-tie-tt1 --role agent
```

remove scaled nodes

```
k3d node delete k3d-dev-tie-tt1-extra-0-0
k3d node delete k3d-dev-tie-tt1-extra-1-0
k3d node delete k3d-dev-tie-tt1-extra-2-0
k3d node delete k3d-dev-tie-tt1-extra-3-0
k3d node delete k3d-dev-tie-tt1-extra-4-0
k3d node delete k3d-dev-tie-tt1-extra-5-0
k3d node delete k3d-dev-tie-tt1-extra-6-0
k3d node delete k3d-dev-tie-tt1-extra-7-0
k3d node delete k3d-dev-tie-tt1-extra-8-0
```