# Cluster Rabbitmq

Para este ejemplo vamos a instalar un cluster de rabbitmq usando el paquete [Helm de bitnami](https://bitnami.com/stack/rabbitmq/helm).

## Deploy

### Crear namespace y añadir repo de bitnami

```
kubectl create ns rmq
helm repo add bitnami https://charts.bitnami.com/bitnami
```

### Deploy rabbitmq

```
helm install --namespace rmq --values values.yaml rmq bitnami/rabbitmq
```

o usando kustomization

```
kubectl kustomize --enable-helm | kubectl apply -f -
```

## Uso de scripts

### Setup port forward

```
kubectl -n rmq port-forward service/rmq-rabbitmq 45672:15672 55672:5672
```

### Acceder a la UI de admin de rabbit

```
http://127.0.0.1:45672
```

user: `admin`
pass: `admin`

### Levantar Consumer

```
./consumer.py --rabbitmq-url 127.0.0.1 --rabbitmq-port 55672 --rabbitmq-destination-queue incremental-messages
```

### Mandar Mensajes

```
publisher.py --rabbitmq-url 127.0.0.1 --rabbitmq-port 55672 --rabbitmq-destination-queue incremental-messages --send-incremental-messages 10000 --send-incremental-messages-threads 10
```
