#!/usr/bin/env python
import pika
import argparse
import time
import math
from concurrent.futures import ThreadPoolExecutor, as_completed
import traceback

parser = argparse.ArgumentParser()
parser.add_argument("--rabbitmq-url", default="testrmq-rabbitmq.rmq.svc.cluster.local" ,help="Rabbitmq Cluster URL")
parser.add_argument("--rabbitmq-port", default=5672 ,help="Rabbitmq Cluster Port")
parser.add_argument("--rabbitmq-user", default="admin" ,help="Rabbitmq Cluster User")
parser.add_argument("--rabbitmq-password", default="admin" ,help="Rabbitmq Cluster Password")
parser.add_argument("--rabbitmq-vhost", default="/" ,help="Rabbitmq Cluster Vhost")
parser.add_argument("--send-incremental-messages",default=0,type=int, help="Send X messages")
parser.add_argument("--rabbitmq-destination-queue",default=None, help="Rabbitmq destination queue name")
parser.add_argument("--send-incremental-messages-delay",default=0,type=int, help="Delay between send-incremental-messages")
parser.add_argument("--send-incremental-messages-threads",default=0,type=int, help="Number of Threads to send-incremental-messages")
parser.add_argument("--debug",action='store_true', help="DEBUG")
parser.add_argument("--no-rabbitmq-confirm-delivery",action='store_true', help="Enable Confirm Delivery")
parser.add_argument("--rabbitmq-destination-queue-type",default='quorum', help="Enable Confirm Delivery")
args = parser.parse_args()

if args.debug:
    print(args)

credentials = pika.PlainCredentials(args.rabbitmq_user, args.rabbitmq_password)

parameters = pika.ConnectionParameters(args.rabbitmq_url,
                                   args.rabbitmq_port,
                                   args.rabbitmq_vhost,
                                   credentials)

def connect(rmq_dst_queue,rmq_dst_queue_type):
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    if not args.no_rabbitmq_confirm_delivery:
        channel.confirm_delivery()
    if rmq_dst_queue_type == "normal":
        channel.queue_declare(queue=rmq_dst_queue)
    elif rmq_dst_queue_type == "quorum":
        channel.queue_declare(queue=rmq_dst_queue,durable=True,arguments={"x-queue-type": "quorum"})

    return(connection,channel)

def send_incremental_messages(sim,start_id,rmq_dst_queue,sim_delay,name="One",debug=False):
    id = start_id
    if rmq_dst_queue is None:
        rmq_dst_queue="incremental-messages"
    if args.debug:
        print(f"Name: {name}\n    sim: {sim}\n    start_id: {start_id}\n    rmq_dst_queue: {rmq_dst_queue}\n    sim_delay: {sim_delay}\n")
    body=f"I'm the message with the id: {id} !"
    index=0

    connection,channel=connect(rmq_dst_queue,args.rabbitmq_destination_queue_type)
    while index < sim:
        previous_state_index=index
        previous_state_body=body

        if not connection.is_open:
            connection,channel=connect(rmq_dst_queue,args.rabbitmq_destination_queue_type)
        if debug:
            print(f"{name} / index: {index} / start_id: {start_id} / body: {body}")
        try:
            channel.basic_publish(exchange='',
                      routing_key=rmq_dst_queue,
                      body=str(body))
            index=index+1
            id=id+1
            body=f"I'm the message with the id: {id} !"
        except Exception as e:
            print(f"ERROR: {name} / index: {index} / previous_state_index: {previous_state_index}\n    body: {body} / previous_state_body: {previous_state_body}\nOops. cant publish message {body}. trying to reconnect.\n\n{e}\n\n\n")
            if not args.no_rabbitmq_confirm_delivery:
                index=index+1
                id=id+1
                body=f"I'm the message with the id: {id} !"
            time.sleep(10)
            continue

        if sim_delay > 0:
                    time.sleep(sim_delay)
    connection.close()

if args.send_incremental_messages > 0:
    if args.send_incremental_messages_threads > 1:
        to_distribute=math.trunc(args.send_incremental_messages/args.send_incremental_messages_threads)
        to_distribute_module=args.send_incremental_messages % args.send_incremental_messages_threads

        values_sim=[to_distribute]*(args.send_incremental_messages_threads-1)
        values_sim.append(to_distribute+to_distribute_module)

        values_start_id=[]
        start_id=0
        for i in range(args.send_incremental_messages_threads):
            values_start_id.append(start_id)
            start_id=start_id+to_distribute

        values_rmq_dst_queue=[args.rabbitmq_destination_queue]*args.send_incremental_messages_threads

        values_sim_delay=[args.send_incremental_messages_delay]*args.send_incremental_messages_threads

        values_debug=[args.debug]*args.send_incremental_messages_threads

        if args.debug:
            print(f"Array for Threads values_sim: {values_sim}")
            print(f"Array for Threads values_start_id: {values_start_id}")
            print(f"Array for Threads values_rmq_dst_queue: {values_rmq_dst_queue}")
            print(f"Array for Threads values_sim_delay: {values_sim_delay}")

        with ThreadPoolExecutor(max_workers=args.send_incremental_messages_threads) as executor:
            future_list = []
            for a in range(args.send_incremental_messages_threads):
                future = executor.submit(send_incremental_messages,values_sim[a],values_start_id[a],values_rmq_dst_queue[a],values_sim_delay[a],f"Thread-{a}",values_debug[a])
                future_list.append(future)
            for f in as_completed(future_list):
                print(f.result())
    else:
        send_incremental_messages(args.send_incremental_messages,0,args.rabbitmq_destination_queue,args.send_incremental_messages_delay,"One",args.debug)
