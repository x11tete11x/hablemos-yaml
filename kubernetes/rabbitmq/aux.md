```bash
rabbitmqctl add_user admin admin
rabbitmqctl change_password admin admin
rabbitmqctl set_user_tags admin administrator
rabbitmqctl set_permissions -p / admin ".*" ".*" ".*"
```

```bash
#1
/usr/bin/time -p ./consumer.py --rabbitmq-url 127.0.0.1 --rabbitmq-port 5672 --rabbitmq-destination-queue incremental-messages-nq --no-rabbitmq-confirm-delivery --rabbitmq-destination-queue-type classic

/usr/bin/time -p ./publisher.py --rabbitmq-url 127.0.0.1 --rabbitmq-port 5672 --rabbitmq-destination-queue incremental-messages-nq --send-incremental-messages 10000 --no-rabbitmq-confirm-delivery --rabbitmq-destination-queue-type classic

#2
/usr/bin/time -p ./consumer.py --rabbitmq-url 127.0.0.1 --rabbitmq-port 5672 --rabbitmq-destination-queue incremental-messages --no-rabbitmq-confirm-delivery

/usr/bin/time -p ./publisher.py --rabbitmq-url 127.0.0.1 --rabbitmq-port 5672 --rabbitmq-destination-queue incremental-messages --send-incremental-messages 10000 --no-rabbitmq-confirm-delivery

#3
/usr/bin/time -p ./consumer.py --rabbitmq-url 127.0.0.1 --rabbitmq-port 5672 --rabbitmq-destination-queue incremental-messages --no-rabbitmq-confirm-delivery

/usr/bin/time -p ./publisher.py --rabbitmq-url 127.0.0.1 --rabbitmq-port 5672 --rabbitmq-destination-queue incremental-messages --send-incremental-messages 10000 --no-rabbitmq-confirm-delivery --send-incremental-messages-threads 20

#4
/usr/bin/time -p  ./consumer.py --rabbitmq-url 127.0.0.1 --rabbitmq-port 5672 --rabbitmq-destination-queue incremental-messages

/usr/bin/time -p ./publisher.py --rabbitmq-url 127.0.0.1 --rabbitmq-port 5672 --rabbitmq-destination-queue incremental-messages --send-incremental-messages 10000 --send-incremental-messages-threads 20
```