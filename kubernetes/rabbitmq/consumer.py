#!/usr/bin/env python
import pika
import argparse
import time
import math
from concurrent.futures import ThreadPoolExecutor, as_completed
import traceback

parser = argparse.ArgumentParser()
parser.add_argument("--rabbitmq-url", default="testrmq-rabbitmq.rmq.svc.cluster.local" ,help="Rabbitmq Cluster URL")
parser.add_argument("--rabbitmq-port", default=5672 ,help="Rabbitmq Cluster Port")
parser.add_argument("--rabbitmq-user", default="admin" ,help="Rabbitmq Cluster User")
parser.add_argument("--rabbitmq-password", default="admin" ,help="Rabbitmq Cluster Password")
parser.add_argument("--rabbitmq-vhost", default="/" ,help="Rabbitmq Cluster Vhost")
parser.add_argument("--rabbitmq-destination-queue",default=None, help="Rabbitmq destination queue name")
parser.add_argument("--debug",action='store_true', help="DEBUG")
parser.add_argument("--no-rabbitmq-confirm-delivery",action='store_true', help="Enable Confirm Delivery")
parser.add_argument("--rabbitmq-destination-queue-type",default='quorum', help="Enable Confirm Delivery")
args = parser.parse_args()

# Define a counter variable
counter = 0

if args.debug:
    print(args)

credentials = pika.PlainCredentials(args.rabbitmq_user, args.rabbitmq_password)

parameters = pika.ConnectionParameters(args.rabbitmq_url,
                                   args.rabbitmq_port,
                                   args.rabbitmq_vhost,
                                   credentials)

def connect(rmq_dst_queue,rmq_dst_queue_type):
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    if not args.no_rabbitmq_confirm_delivery:
        channel.confirm_delivery()
    if rmq_dst_queue_type == "normal":
        channel.queue_declare(queue=rmq_dst_queue)
    elif rmq_dst_queue_type == "quorum":
        channel.queue_declare(queue=rmq_dst_queue,durable=True,arguments={"x-queue-type": "quorum"})

    return(connection,channel)

# Define callback function
def callback(ch, method, properties, body):
    global counter
    print(" [x] Received %r" % str(body))
    counter += 1
    print(f'Number of messages processed: {counter}')

# Define consuming function
def start_consuming(channel,queue):
    channel.basic_consume(queue=queue, on_message_callback=callback, auto_ack=True)
    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()

connection,channel=connect(args.rabbitmq_destination_queue,args.rabbitmq_destination_queue_type)
start_consuming(channel,args.rabbitmq_destination_queue)