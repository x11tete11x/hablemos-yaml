# Minio
[Documentación oficial](https://github.com/minio/operator/blob/master/README.md)

## Pre requisitos
* [Krew](https://krew.sigs.k8s.io/)

## Instalación
```bash
kubectl krew update
kubectl krew install minio
kubectl minio init
kubectl create ns minio-ns
kubectl minio tenant create tenant1 --servers 3 --volumes 6 --capacity 18Gi --namespace minio-ns --storage-class local-path
```

## Desinstalación
```bash
kubectl minio tenant delete tenant1 -n minio-ns
kubectl delete ns minio-ns
kubectl minio delete
kubectl delete csr operator-minio-operator-csr
kubectl delete csr tenant1-client-minio-ns-csr
kubectl delete csr tenant1-console-minio-ns-csr
kubectl delete csr tenant1-kes-minio-ns-csr
kubectl delete csr tenant1-minio-ns-csr
```

# SFTPGO
[SFTPGO](https://github.com/drakkan/sftpgo) es un servidor SFTP lleno de características y completamente configurable, compatible con varios backends de almacenamiento,entre ellos Amazon S3

## Instalación
```bash
kubectl create -f sftpgo.yaml --namespace minio-ns
```

## Desinstalación
```bash
kubectl delete -f sftpgo.yaml --namespace minio-ns
```

# Configuración SFTPGO - MINIO

## Consola de Minio

Accedemos a la consola y creamos:

* Bucket: `testbucket`
* User / Access Key: `testminiouser`
* User / Access Secret: `testminiouser123456`
* User / Policy: `consoleAdmin`

## SFTPGO
Accedemos a la consola de SFTPGO y creamos:
* Username: `testsftpuser`
* Password: `testsftpuser123456`
* Permissions: `*`
* Storage: `AWS S3 (Compatible)`
* Bucket: `testbucket`
* Region: `us-east-1`
* Access Key: `testminiouser`
* Access Secret: `testminiouser123456`
* Endpoint: `minio.minio-ns.svc.cluster.local`

# Pruebas
Para nuestras pruebas vamos a deployar un pod con herramientas (busybox) y nos vamos a conectar por sftp al sftpgo, subir cosas, y ver que éste último las guarda en el storage distribuido de minio.

## Deploy busybox
```bash
kubectl create -f busybox.yaml --namespace minio-ns
```

## Pruebas

* Dentro del pod de busybox:
```bash
echo "Test File" >> testfile.txt
fallocate -l 1G 1G.out
fallocate -l 300M 300M.out
sftp -P 2022 testsftpuser@sftpgo-service.minio-ns.svc.cluster.local
ls
put 1G.out
ls
```

* Entramos a la consola de Minio y en el "Object Browser" podemos ver que el archivo creado por el SFTPGO se encuentra dentro del bucket de minio

* Matar un pod de minio y repetir la prueba.
