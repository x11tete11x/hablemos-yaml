# Wordpress en Kubernetes
## Deploy
Posicionados en la carpeta donde etá el `kustomization.yaml`
```bash
kubectl create ns wordpress
kubectl apply -k ./ --namespace wordpress
```
## Escalar
```bash
kubectl scale deployment.v1.apps/wordpress --replicas=10 --namespace wordpress
```
