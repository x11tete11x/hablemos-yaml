# Requerimientos
* [K3D](https://k3d.io/)
* [Lens](https://k8slens.dev/)
* [Kubectl](https://kubernetes.io/es/docs/tasks/tools/install-kubectl/)
* [Krew](https://krew.sigs.k8s.io/)
* [MinIO](https://docs.min.io/docs/deploy-minio-on-kubernetes.html)

# K3D
## Instalación
Referirse a la [documentación oficial](https://k3d.io/)

## Corriendo K3D
En éste caso lo vamos a arrancar de la siguiente manera:

```bash
k3d cluster create test --servers 3 --api-port IP:6443 --agents 2 -p "8081:80@loadbalancer"
```

Lo que nos deja un cluster con la siguientes caracteisticas:

* 3 servers
* 2 agentes (éstos son unos pods para exponer servicios)
* La API para conectarse al cluster expuesta bajo "IP:6443"
* El 8081 mapeado al balanceador interno del cluster K3D (para exponer servicios para afuera usando el balanceador)

## Conectarse al Cluster K3D
Luego de arrancar el cluster copiamos la configuración del mismo:

```bash
[x11tete11x@warmachine-tt1 ~]$ cat .kube/config
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: CERT
    server: https://IP:6443
  name: k3d-test
contexts:
- context:
    cluster: k3d-test
    user: admin@k3d-test
  name: k3d-test
current-context: k3d-test
kind: Config
preferences: {}
users:
- name: admin@k3d-test
  user:
    client-certificate-data: CERT
    client-key-data: CERT KEY
```

Y la pegamos en el IDE de Lens:

![Thumper](img/add-cluster.png)

Finalmente estamos listos:

![Thumper](img/cluster.png)

# Información relevante
* [K3D exposing services](https://k3d.io/usage/guides/exposing_services/)
* [Wordpress Deploy](https://kubernetes.io/docs/tutorials/stateful-application/mysql-wordpress-persistent-volume/)
* [Service vs Deployment](https://matthewpalmer.net/kubernetes-app-developer/articles/service-kubernetes-example-tutorial.html)
* [StatefulSet Basics](https://kubernetes.io/docs/tutorials/stateful-application/basic-stateful-set/)
* [Secrets](https://kubernetes.io/docs/concepts/configuration/secret/)
