# Instalar docker-compose
[https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/)

# Montar un Wordpress
* Escribimos el siguiente [YAML](./wordpress/docker-compose.yaml) para levantar un Wordpress + DB mysql
* Levantamos el entorno, para ello, posicionados en la carpeta donde esta el YAML:

```bash
docker-compose up -d
```

__NOTAR:__ Wordpress se comunica con la DB __mediante la red interna creada por docker__ el __único__ puerto expuesto para afuera es el 40080 que se mapea al 80 del container que corre Wordpress
