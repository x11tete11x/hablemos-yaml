# Armando una caja de herramientas
[Dockerfile de busybox](busybox/Dockerfile)

# Buildeando imagenes
Nos posicionamos en la carpeta donde está nuestro Dockerfile y:
```bash
docker build . -t busybox
```

# Corriendo container a partir de imagen
```bash
docker run -ti busybox
```

# Corriendo otros sistemas operativos

## Fedora
```bash
docker run -ti fedora bash
cat /etc/os-release
```

## Archlinux
```bash
docker run -ti archlinux bash
cat /etc/os-release
```

## Ubuntu
```bash
docker run -ti ubuntu bash
cat /etc/os-release
```

## Alpine
```bash
docker run -ti alpine sh
cat /etc/os-release
```
